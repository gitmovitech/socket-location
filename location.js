var request = require('request');
var io = require('socket.io')(8085);
var StaticServer = require('static-server');
var server = new StaticServer({
    rootPath: '.',
    port: 1337,
    cors: '*'
});

io.on('connection', function (socket) {
    console.log('Usuario conectado');

    socket.on('locationSave', function (data) {
        console.log(data.data.lat, data.data.lng);
        request({
            url: "http://162.208.9.10:6842?latlng=" + data.data.lat + "," + data.data.lng + "&ts=" + (new Date().getTime() / 1000) + "&or=0&sp=0&oc=true",
            method: "POST",
            json: true,
            body: {},
            headers: {
                "Content-Type": "application/json",
                "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJydW4iOiIxNTI5OTcyMy1LIiwidHlwZSI6ImRyaXZlciIsImlhdCI6MTUzMjgxMjczNX0.U8nu7K4RNJuQHCDOlnAiqCoinumGkTL1bqrglu7tjMA"
            }
        }, function (error, response, body) { });
        socket.broadcast.emit('locationSave', data);
    });

    socket.on('userConnected', function (data) {
        socket.broadcast.emit('userConnected', data);
    })

    socket.on('locationSent', function (data) {
        console.log('locationSent');
        console.log(data);
        socket.broadcast.emit('locationSent', data);
    });

    socket.on('getLocalStorage', function (data, cb) {
        console.log('paso al servidor')
        socket.broadcast.emit('getLocalStorage', data);
    })

    socket.on('disconnect', function () {
        console.log('Usuario desconectado')
    })
});

server.start(function () {
    console.log('Server listening to', server.port);
});